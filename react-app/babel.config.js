module.exports = {
  presets: [
    [
      '@babel/preset-env', {
        targets: '> 0.25%, not dead',
        shippedProposals: true, // https://babeljs.io/docs/en/babel-preset-env#shippedproposals
      },
    ],
      '@babel/preset-react'
  ],
}
