function convert(input){
	var i = 0;
	var output = "";
	var padding = 0;
	while(i<input.length){
		padding = 3 - input.charCodeAt(i).toString().length;
		while(padding > 0){
			output += "0";
			padding--;
		}
		output += input.charCodeAt(i).toString();
		i++;
	}
}

print(convert("nrX-t83D0yU"))