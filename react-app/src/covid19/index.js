const { Button, Nav, Navbar, NavbarBrand, NavLink, NavbarText, Form, Table } = window.Reactstrap

class CovidComponent extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      searchTerm: "Austin",
      errorMsg: "",
      searchResults: [],
    }

    this.validInput = this.validInput.bind(this);
    this.searchCovidAPI = this.searchCovidAPI.bind(this);
  }

  handleChange (event) {
    this.setState( { searchTerm: event.target.value } )
  }

  handleClick() {
    // Clear fields
    this.setState( { errorMsg: "", searchResults: [] } )

    console.log(this.state.searchTerm);

    // Attempt to update fields
    if (this.validInput()) {
      this.searchCovidAPI()
    }
  }


  // Check if user input exists and update error message accordingly
  validInput() {
    if(this.state.searchTerm == "") {
      // Empty input - set error message
      this.setState( { errorMsg: "Please provide a city to lookup." } )
      return false
    }
    else {
      // Reset error message
      this.setState( { errorMsg: "" } )
      return true
    }
  }

  searchCovidAPI() {
    console.log("searchCovidAPI")
    var request = new XMLHttpRequest()
    var url = "http://quarantinefighters.me/api/City/?name="+this.state.searchTerm
    request.open('GET', url, true)
    request.onload = function () {
      var response = JSON.parse(request.responseText)
      console.log(response)

      // The API always returns HTTP 200 code.
      // A Bad response is distinguished by an empty response
      if(response.hasOwnProperty("cities")){
        var cities = response["cities"]
        if(cities.length > 0) {
          this.setState( { searchResults: cities } )
        }
        else {
          this.setState( { errorMsg: "Unable to find results for city '"+this.state.searchTerm+"'. Try capitalizing the first letter of the city." } )
        }
      } else {
        console.log('error')
        this.setState( { errorMsg: "Unknown city '"+this.state.searchTerm+"'. Try capitalizing the first letter of the city." } )
      }

    }.bind(this)

    request.send()
  }

  render() {

    var cityResults = this.state.searchResults.map((city, index) => (
      <div className="city" key={city["id"]}>
        <p>Results for { city["name"] }, { city["state"] }:</p>
        <ul>
          <li>{ city["drugstores"].length } drugstores found.</li>
          <li>{ city["hospitals"].length } hospitals found.</li>
        </ul>
      </div>
    ))


    return (
      <div className="covid">

        <p>Find Covid-19 resources by city </p>

        <div id="search">
          <label htmlFor="city">City:</label>
          <input type="text" id="city" name="city"
            value={this.state.searchTerm}
            onChange={this.handleChange.bind(this)} />
          <button onClick={ this.handleClick.bind(this) }>Search</button>
          <div id="error">{this.state.errorMsg}</div>
        </div>

        { this.state.searchResults.length > 0 &&
          <div id="result">
            { cityResults }
            <a href="http://quarantinefighters.me/#/">Learn more about COVID-19 resources.</a>
          </div>
        }

      </div>
    )
  }
}


ReactDOM.render(
  <React.StrictMode>
    <div className="App" style={{ fontFamily: 'Consolas' }}>
      <div>
    		<Navbar color="white" light expand="sm">
    			<NavbarBrand href="/"> photoslayers </NavbarBrand>
          <Nav className="mr-auto" navbar> </Nav>
          <NavbarText>
            <NavLink href="/search" style={{ color: 'black' }}>
              Search
            </NavLink>
          </NavbarText>
    			<NavbarText> <NavLink href="/photos" style={{ color: 'black' }}> Photos </NavLink> </NavbarText>
    			<NavbarText> <NavLink href="/photographers" style={{ color: 'black' }}> Photographers </NavLink> </NavbarText>
    			<NavbarText> <NavLink href="/collections" style={{ color: 'black' }}> Collections </NavLink> </NavbarText>
    			<NavbarText> <NavLink href = "/about" style={{ color: 'black' }}> About </NavLink> </NavbarText>
    		</Navbar>
  	  </div>

      <CovidComponent />
    </div>
  </React.StrictMode>,
  document.getElementById('root')
)
