const {
  Badge,
  Button,
  Nav,
  Navbar,
  NavbarBrand,
  NavLink,
  NavbarText,
  Form,
  Table,
  Media,
  Input,
  ListGroup,
  ListGroupItem,
} = window.Reactstrap
var imageSize = 200
var buttonIcons = {
  unsorted: '../assets/unsorted.png',
  ascending: '../assets/ascending.png',
  descending: '../assets/descending.png',
}

class SearchPageComponent extends React.Component {
  state = {
    itemsPerPage: 3,
    activePageNum: 0,
    pageStatus: '',

    data: [],
    items: [],

    searchTerm: '',
  }

  componentDidMount() {
    this.setState({ searchTerm: new URLSearchParams(window.location.search).get('q') })

    const standardizedItems = [
      ...this.props.data.photos.map((photo) => ({
        href: '/photos/' + photo.id,
        heading: `Image by ${photo.user.name}`,
        image: photo.urls.raw,
        body: [photo.description || photo.alt_description || 'no description'],
        meta: [
          convertDate(photo.created_at),
          `${photo.height} x ${photo.width}`,
          <Badge color="primary" pill>
            <ion-icon name="thumbs-up-outline"></ion-icon> {photo.likes}
          </Badge>,
        ],
        matches: photo.matches,
      })),
      ...this.props.data.photographers.map((photographer) => ({
        href: '/photographers/' + photographer.id,
        heading: photographer.name,
        image: photographer.profile_image.large,
        body: [photographer.bio],
        meta: [
          <>
            {photographer.instagram_username && (
              <Badge color="primary" pill>
                <ion-icon name="logo-instagram"></ion-icon>{' '}
                {photographer.instagram_username}
              </Badge>
            )}
            {photographer.twitter_username && (
              <Badge color="primary" pill>
                <ion-icon name="logo-twitter"></ion-icon>{' '}
                {photographer.twitter_username}
              </Badge>
            )}
          </>,
        ],
        matches: photographer.matches,
      })),
      ...this.props.data.collections.map((collection) => ({
        href: '/collections/' + collection.id,
        heading: `${collection.title} by ${collection.user.name}`,
        image: collection.cover_photo.urls.raw,
        body: [
          `total photos: ${collection.total_photos}`,
          <br />,
          `updated: ${convertDate(collection.updated_at)}`,
        ],
        meta: [
          <>
            {collection.tags.map((tag) => (
              <Badge color="primary" pill>
                {tag.title}
              </Badge>
            ))}
          </>,
        ],
        matches: collection.matches,
      })),
      // put in order of best match
    ].sort((a, b) => b.matches.length - a.matches.length)
    console.log(
      `SearchPageComponent -> componentDidMount -> standardizedItems`,
      standardizedItems
    )

    this.setState({
      data: standardizedItems,
      items: standardizedItems.slice(
        this.state.activePageNum * this.state.itemsPerPage,
        this.state.activePageNum * this.state.itemsPerPage + this.state.itemsPerPage
      ),
      pageStatus: `Displaying page ${this.state.activePageNum + 1} of ${Math.ceil(
        standardizedItems.length / this.state.itemsPerPage
      )}, ${this.state.itemsPerPage} entries per page`,
    })
  }

  flipPage(direction) {
    if (direction === 'back' && this.state.activePageNum > 0) {
      this.refreshPage(this.state.activePageNum - 1)
    } else if (
      direction === 'forward' &&
      this.state.activePageNum < this.state.data.length / this.state.itemsPerPage - 1
    ) {
      console.log(`SearchPageComponent -> flipPage -> direction`, direction)
      this.refreshPage(this.state.activePageNum + 1)
    }
  }

  selectPage() {
    var number = parseInt(document.getElementById('pageSelection').value)
    if (number > 1 && number < this.state.data.length / this.state.itemsPerPage + 1) {
      this.refreshPage(number - 1)
    }
  }

  editPageEntries() {
    var number = document.getElementById('entriesPerPage').value
    if (number > 0 && number <= this.state.items.length) {
      this.state.itemsPerPage = number
      this.state.activePageNum = 0
    }
    this.refreshPage()
  }

  refreshPage(destinationPageNum) {
    this.state = {
      activePageNum: destinationPageNum,
      items: this.state.data.slice(
        destinationPageNum * this.state.itemsPerPage,
        destinationPageNum * this.state.itemsPerPage + this.state.itemsPerPage
      ),
      pageStatus: `Displaying page ${destinationPageNum + 1} of ${Math.ceil(
        this.state.data.length / this.state.itemsPerPage
      )}, ${this.state.itemsPerPage} entries per page`,
    }
    document.getElementById('headerForm').reset()
    this.setState(this.state)
  }

  // refresh page with new query params
  search = () => {
    window.location.href = `${window.location.origin}${window.location.pathname}?q=${this.state.searchTerm}`
  }

  render() {
    return (
      <div className="photo">
        <div className="search-header">
          <form id="headerForm">
            <span class="b">{this.state.pageStatus}</span>
            <span className="a">Search</span>
            <span className="c">
              <label>
                Page Number:
                <input
                  type="text"
                  id="pageSelection"
                  size="5"
                  placeholder={this.state.activePageNum + 1}
                />
              </label>
              <button type="button" onClick={() => this.selectPage()}>
                Jump
              </button>
              <button type="button" onClick={() => this.flipPage('back')}>
                Back
              </button>
              <button type="button" onClick={() => this.flipPage('forward')}>
                Forward
              </button>
            </span>
          </form>
        </div>
        <div className="search-body">
          <div className="search-field">
            <Input
              placeholder="enter search term..."
              onChange={(event) => this.setState({ searchTerm: event.target.value })}
              value={this.state.searchTerm}
            ></Input>
            <Button onClick={this.search}>Search</Button>
          </div>
          <ListGroup flush>
            {this.state.items.map((obj) => (
              <ListGroupItem tag="a" href={obj.href}>
                <Media className="search-item">
                  <Media left>
                    <Media
                      object
                      className="search-image"
                      src={obj.image}
                      alt="Generic placeholder image"
                    />
                  </Media>
                  <Media body>
                    <Media heading>{obj.heading}</Media>
                    <div className="search-item-body">{obj.body}</div>

                    <div className="search-item-meta">
                      {obj.meta.map((line) => (
                        <>
                          {line}
                          <br />
                        </>
                      ))}
                      {`(matches: ${obj.matches.join(', ')})`}
                    </div>
                  </Media>
                </Media>
              </ListGroupItem>
            ))}
          </ListGroup>
        </div>
      </div>
    )
  }
}

function convert(input) {
  var i = 0
  var output = ''
  var padding = 0
  while (i < input.length) {
    padding = 3 - input.charCodeAt(i).toString().length
    while (padding > 0) {
      output += '0'
      padding--
    }
    output += input.charCodeAt(i).toString()
    i++
  }
  return output
}

function convertDate(s) {
  var date = new Date(s)
  return date.toDateString()
}

function orientation(obj) {
  if (obj.width > obj.height) {
    return 'Landscape'
  }
  if (obj.width < obj.height) {
    return 'Potrait'
  } else {
    return 'Square'
  }
}

// retrieve display data that flask passed via html for us
const data = document.querySelector('#flask-data').dataset
const json = JSON.parse(data.json)

ReactDOM.render(
  <React.StrictMode>
    <div className="App" style={{ fontFamily: 'Consolas' }}>
      <div>
        <Navbar color="white" light expand="sm">
          <NavbarBrand href="/"> photoslayers </NavbarBrand>
          <NavbarText>
            <NavLink href="/covid19" style={{ color: 'orange' }}>
              COVID-19 Resources
            </NavLink>
          </NavbarText>
          <Nav className="mr-auto" navbar></Nav>
          <NavbarText>
            <NavLink href="/search" style={{ color: 'black' }}>
              Search
            </NavLink>
          </NavbarText>
          <NavbarText>
            <NavLink href="/photos" style={{ color: 'black' }}>
              Photos
            </NavLink>
          </NavbarText>
          <NavbarText>
            <NavLink href="/photographers" style={{ color: 'black' }}>
              Photographers
            </NavLink>
          </NavbarText>
          <NavbarText>
            <NavLink href="/collections" style={{ color: 'black' }}>
              Collections
            </NavLink>
          </NavbarText>
          <NavbarText>
            <NavLink href="/about" style={{ color: 'black' }}>
              About
            </NavLink>
          </NavbarText>
        </Navbar>
      </div>
      <SearchPageComponent data={json} />
    </div>
  </React.StrictMode>,
  document.getElementById('root')
)
