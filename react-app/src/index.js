const { Button, Nav, Navbar, NavbarBrand, NavLink, NavItem, NavbarText, Form, Card, CardText, CardTitle, Row, Col } = window.Reactstrap


const HomePage = () => {
	const [searchTerm, setSearchTerm] = React.useState("")

	const search = () => {
		window.location.href = `${window.location.origin}/search/?q=${searchTerm}`
	}

	return (

  <React.StrictMode>
    <div className="App" style={{ fontFamily: 'Consolas' }}>
	<div>
		<Navbar color="white" light expand="sm">
			<NavbarBrand href="/"> photoslayers </NavbarBrand>
      <NavbarText> <NavLink href="/covid19" style={{ color: 'orange' }}> COVID-19 Resources</NavLink> </NavbarText>
			<Nav className="mr-auto" navbar> </Nav>
			<NavbarText> <NavLink href = "/search" style={{ color: 'black' }}> Search </NavLink> </NavbarText>
			<NavbarText> <NavLink href="/photos" style={{ color: 'black' }}> Photos </NavLink> </NavbarText>
			<NavbarText> <NavLink href="/photographers" style={{ color: 'black' }}> Photographers </NavLink> </NavbarText>
			<NavbarText> <NavLink href="/collections" style={{ color: 'black' }}> Collections </NavLink> </NavbarText>
			<NavbarText> <NavLink href = "/about" style={{ color: 'black' }}> About </NavLink> </NavbarText>
		</Navbar>
	</div>

	<div>
		<br/>
		<Col sm={{ size: 2, offset: 5 }}>
		<Card>
			<CardText tag='h2'>Welcome!</CardText>
		</Card>
		</Col>
		<br/>

		<Row>
		<Col sm={{ size: 2, offset: 3 }}>
			<Card body>
			  <CardText>Explore a catalogue of photographs.</CardText>
			  <Button outline color="success" href="/photos"> Photos </Button>
			</Card>
		</Col>
		<Col sm="2">
			<Card body>
			  <CardText>Get to know the eyes behind the photos.</CardText>
			  <Button outline color="warning" href="/photographers"> Photographers </Button>
			</Card>
		</Col>
		<Col sm="2">
			<Card body>
			  <CardText>Check out these albums created by users.</CardText>
			  <Button outline color="danger" href="/collections"> Collections </Button>
			</Card>
		</Col>
		</Row>
    </div>

	<div>
		<br/>
		<Form>
		   <input type="text" placeholder="enter search term..." size="50" value={searchTerm} onChange={(event) => setSearchTerm(event.target.value)} />
		</Form>
		<br />
		<Col sm={{ size: 2, offset: 5 }}>
			<Card>
			  <Button outline color="secondary" onClick={search}>SEARCH</Button>
			</Card>
		</Col>
	</div>

		</div>
		</React.StrictMode>
	)
}

ReactDOM.render(
	<HomePage />,
  document.getElementById('root')
)
