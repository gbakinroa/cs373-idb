const { Button, Nav, Navbar, NavbarBrand, NavLink, NavbarText, Form, Table } = window.Reactstrap
var imageSize = 200
var buttonIcons = {
  "unsorted": "../assets/unsorted.png",
  "ascending": "../assets/ascending.png",
  "descending": "../assets/descending.png",
}

class PhotosPageComponent extends React.Component {

  previousKey = 'user[name]'
  pageEntries = 3
  pageNumber = 0
  totalEntries = this.props.data.photos.length

  state = {
    input: [],
    photos: [],
    buttons: [],
    pageStatus: "",
  }

  componentDidMount() {
    this.setState({
      input: this.props.data.photos,
      photos: this.props.data.photos.slice(
        this.pageNumber * this.pageEntries,
        this.pageNumber * this.pageEntries + this.pageEntries
      ),
      buttons: {'user[name]': "unsorted", 'orientation': "unsorted", 'likes': "unsorted", 'created_at': "unsorted", 'updated_at': "unsorted"},
      pageStatus: "Displaying page " + this.pageNumber + " of " + Math.ceil((this.totalEntries/this.pageEntries)-1) + ", " + this.pageEntries + " entries per page",
    })
  }

  onSort(sortKey){
    const data = this.state.input;
    const buttonData = this.state.buttons;
    var x = -1;
    var y = 1;
    if(this.previousKey != sortKey){
      buttonData[this.previousKey] = 'unsorted';
    }
    if(buttonData[sortKey] == 'ascending'){
      x = 1;
      y = -1;
      buttonData[sortKey] = 'descending';
    }
    else{
      buttonData[sortKey] = 'ascending';
    }
    this.previousKey = sortKey;
    if(sortKey=='created_at' || sortKey=='updated_at'){
      data.sort((a,b) => {
        if (Date(a[sortKey]) < Date(b[sortKey])) {
          return x;
        }
        if (Date(a[sortKey]) > Date(b[sortKey])) {
          return y;
        }
        return 0;
      });
    }
    else if(sortKey=="orientation"){
      data.sort((a,b) => {
        if (orientation(a) < orientation(b)) {
          return x;
        }
        if (orientation(a) > orientation(b)) {
          return y;
        }
        return 0;
      });
    }
    else if(sortKey=="user[name]"){
      data.sort((a,b) => {
        if (a.user['name'] < b.user['name']) {
          return x;
        }
        if (a.user['name'] > b.user['name']) {
          return y;
        }
        return 0;
      });
    }
    else{
      data.sort((a,b) => {
        if (a[sortKey] < b[sortKey]) {
          return x;
        }
        if (a[sortKey] > b[sortKey]) {
          return y;
        }
        return 0;
      });
    }
    this.state = { 'photos': data.slice(this.pageNumber*this.pageEntries,(this.pageNumber*this.pageEntries) + this.pageEntries),
                   'buttons': buttonData,};
    this.setState(this.state);
  }

  flipPage(direction){
    if(direction == 'back' && this.pageNumber!=0){
      this.pageNumber--;
    }
    else if(direction == 'forward' && (this.pageNumber < (this.totalEntries/this.pageEntries) - 1)){
      this.pageNumber++;
    }
    this.refreshPage();
  }

  selectPage(){
    var number = parseInt(document.getElementById("pageSelection").value);
    if((number >= 0) && (number < (this.totalEntries/this.pageEntries))){
      this.pageNumber = number;
    }
    this.refreshPage();
  }

  editPageEntries(){
    var number = document.getElementById("entriesPerPage").value;
    if((number > 0) && (number <= this.totalEntries)){
      this.pageEntries = number;
      this.pageNumber = 0;
    }
    this.refreshPage();
  }

  refreshPage(){
    this.state = { 'photos': this.state.input.slice(this.pageNumber*this.pageEntries,(this.pageNumber*this.pageEntries) + this.pageEntries),
                   'pageStatus': "Displaying page " + this.pageNumber + " of " + Math.ceil((this.totalEntries/this.pageEntries)-1) + ", " + this.pageEntries + " entries per page"};
    document.getElementById("headerForm").reset();
    this.setState(this.state);
  }

  render() {
    var sortedObjects = this.state.photos;
    return (
      <div className="photo">
      <form id="headerForm"><span class="b">{this.state.pageStatus}</span>
          <span className="a">Photos</span>
          <span className="c">
          <label>
            Page Number:  <input type="text" id="pageSelection" size='5' placeholder={this.pageNumber}/>
          </label>
          <button type="button" onClick={e => this.selectPage()}>Jump</button>
          <button type="button" onClick={e => this.flipPage('back')}>Back</button>
          <button type="button" onClick={e => this.flipPage('forward')}>Forward</button></span></form>
        <Table>
          <thead>
            <tr>
              <th>Photo</th>
              <th>
                Photographer  <button type="button" onClick={e => this.onSort('user[name]')}>
                <img id='user[name]' src={buttonIcons[this.state.buttons['user[name]']]}
                width='20' height='20'/></button>
              </th>
              <th>
                Orientation  <button type="button" onClick={e => this.onSort('orientation')}>
                <img id='orientation' src={buttonIcons[this.state.buttons['orientation']]}
                width='20' height='20'/></button>
              </th>
              <th>
                Likes  <button type="button" onClick={e => this.onSort('likes')}>
                <img id='likes' src={buttonIcons[this.state.buttons['likes']]}
                width='20' height='20'/></button>
              </th>
              <th>
                Date Created  <button type="button" onClick={e => this.onSort('created_at')}>
                <img id='created_at' src={buttonIcons[this.state.buttons['created_at']]}
                width='20' height='20'/></button>
              </th>
              <th>
                Date Updated  <button type="button" onClick={e => this.onSort('updated_at')}>
                <img id='updated_at' src={buttonIcons[this.state.buttons['updated_at']]}
                width='20' height='20'/></button>
              </th>
            </tr>
          </thead>
          <tbody>
            {
              sortedObjects.map((obj, index) => {
                return (
                  <tr height={imageSize+25}>
                    <th scope="row" width={imageSize+25}>
                    <a href = {"/photos/" + obj.id}>
                    <img class='a' src={obj.urls['raw']}/></a></th>
                    <td width={imageSize}><a href={"/photographers/" + convert(obj.user['id'])}>{ obj.user['name'] }</a></td>
                    <td width={imageSize}>{ orientation(obj) }</td>
                    <td width={imageSize}>{ obj.likes }</td>
                    <td width={imageSize}>{ convertDate(obj.created_at) }</td>
                    <td width={imageSize}>{ convertDate(obj.updated_at) }</td>
                  </tr>
                )
              })
            }
          </tbody>
        </Table>
      </div>
    );
  }
}

function convert(input){
	var i = 0;
	var output = "";
	var padding = 0;
	while(i<input.length){
		padding = 3 - input.charCodeAt(i).toString().length;
		while(padding > 0){
			output += "0";
			padding--;
		}
		output += input.charCodeAt(i).toString();
		i++;
	}
	return output;
}

function convertDate(s) {
  var date = new Date(s);
	return date.toDateString();
}

function orientation(obj){
  if(obj.width > obj.height){
    return "Landscape";
  }
  if(obj.width < obj.height){
    return "Potrait";
  }
  else{
    return "Square";
  }
}

// retrieve display data that flask passed via html for us
const data = document.querySelector("#flask-data").dataset
const json = JSON.parse(data.json)

ReactDOM.render(
  <React.StrictMode>
    <div className="App" style={{ fontFamily: 'Consolas' }}>
    <div>
		<Navbar color="white" light expand="sm">
			<NavbarBrand href="/"> photoslayers </NavbarBrand>
      <NavbarText> <NavLink href="/covid19" style={{ color: 'orange' }}> COVID-19 Resources</NavLink> </NavbarText>
      <Nav className="mr-auto" navbar> </Nav>
      <NavbarText>
        <NavLink href="/search" style={{ color: 'black' }}>
          Search
        </NavLink>
      </NavbarText>
			<NavbarText> <NavLink href="/photos" style={{ color: 'black' }}> Photos </NavLink> </NavbarText>
			<NavbarText> <NavLink href="/photographers" style={{ color: 'black' }}> Photographers </NavLink> </NavbarText>
			<NavbarText> <NavLink href="/collections" style={{ color: 'black' }}> Collections </NavLink> </NavbarText>
			<NavbarText> <NavLink href = "/about" style={{ color: 'black' }}> About </NavLink> </NavbarText>
		</Navbar>
	</div>
    <PhotosPageComponent data={json} />
    </div>
  </React.StrictMode>,
  document.getElementById('root')
)
