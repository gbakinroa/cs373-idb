
const { Button, Nav, Navbar, NavbarBrand, NavLink, NavbarText, Form, Table } = window.Reactstrap
var imageSize = 200

function convert(input){
	var i = 0;
	var output = "";
	var padding = 0;
	while(i<input.length){
		padding = 3 - input.charCodeAt(i).toString().length;
		while(padding > 0){
			output += "0";
			padding--;
		}
		output += input.charCodeAt(i).toString();
		i++;
	}
	return output;
}

class PhotosPageComponent extends React.Component {
  render() {
	const {data} = this.props
	
	var i=1;
	var tags = [];
	if(data.tags.length != 0){
		tags += data.tags[0].title;
		while(i<data.tags.length){
			tags += ", " + data.tags[i].title ;
			i++;
		}
	}

    return (
      <div className="photo">
	  	<span className="a">Photos</span>
		<div className='columna' align="right">
		<img className='b' src={data.urls['raw']}/></div>
		<div className='columnb' align="left">
			<Table>
			<tbody>
						<table>
						<tr>
							<th>Photographer</th>
							<td><a href={"/photographers/" + convert(data.user['id'])}>{ data.user['name'] }</a></td>
						</tr>
						<tr>
							<th>Orientation</th>
							<td>{ orientation(data) }</td>
						</tr>
						<tr>
							<th>Likes</th>
							<td>{ data.likes }</td>
						</tr>
						<tr>
							<th>Date Created</th>
							<td>{ convertDate(data.created_at) }</td>
						</tr>
						<tr>
							<th>Date Updated</th>
							<td>{ convertDate(data.updated_at) }</td>
						</tr>
						<tr>
							<th>Tags</th>
							<td>{ tags }</td>
						</tr>
						</table>
			</tbody>
			</Table>
		</div></div>
    );
  }
}

function convertDate(s) {
	var date = new Date(s);
	return date.toDateString();
}

function orientation(obj){
  if(obj.width > obj.height){
    return "Landscape";
  }
  if(obj.width < obj.height){
    return "Potrait";
  }
  else{
    return "Square";
  }
}

const data = document.querySelector("#flask-data").dataset
const json = JSON.parse(data.json)

ReactDOM.render(
  <React.StrictMode>
    <div className="App" style={{ fontFamily: 'Consolas' }}>
    <div>
		<Navbar color="white" light expand="sm">
			<NavbarBrand href="/"> photoslayers </NavbarBrand>
			<Nav className="mr-auto" navbar> </Nav>		
			<NavbarText> <NavLink href="/photos" style={{ color: 'black' }}> Photos </NavLink> </NavbarText>
			<NavbarText> <NavLink href="/photographers" style={{ color: 'black' }}> Photographers </NavLink> </NavbarText>
			<NavbarText> <NavLink href="/collections" style={{ color: 'black' }}> Collections </NavLink> </NavbarText>
			<NavbarText> <NavLink href = "/about" style={{ color: 'black' }}> About </NavLink> </NavbarText>
		</Navbar>
	</div>

    <PhotosPageComponent data={json} />
    </div>
  </React.StrictMode>,
  document.getElementById('root')
)
