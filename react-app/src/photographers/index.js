const { Button, Nav, Navbar, NavbarBrand, NavLink, NavbarText, Form, Table } = window.Reactstrap
var imageSize = 200
var buttonIcons = {
  "unsorted": "../assets/unsorted.png",
  "ascending": "../assets/ascending.png",
  "descending": "../assets/descending.png",
}

class PhotographersPageComponent extends React.Component {

  previousKey = 'name'
  pageNumber = 0
  pageEntries = 3
  totalEntries = this.props.data.photographers.length

  state = {
    input: [],
    photographers: [],
    buttons: [],
    pageStatus: "",
  }

  componentDidMount() {
    this.setState({
      input: this.props.data.photographers,
      photographers: this.props.data.photographers.slice(
        this.pageNumber * this.pageEntries,
        this.pageNumber * this.pageEntries + this.pageEntries
      ),
      buttons: {'name': "unsorted", 'updated_at': "unsorted", 'total_likes': "unsorted", 'total_photos': "unsorted", 'total_collections': "unsorted"},
      pageStatus: "Displaying page " + this.pageNumber + " of " + Math.ceil((this.totalEntries/this.pageEntries)-1) + ", " + this.pageEntries + " entries per page",
    })
  }

  onSort(sortKey){
    const data = this.state.input;
    const buttonData = this.state.buttons;
    var x = -1;
    var y = 1;
    if(this.previousKey != sortKey){
      buttonData[this.previousKey] = 'unsorted';
    }
    if(buttonData[sortKey] == 'ascending'){
      x = 1;
      y = -1;
      buttonData[sortKey] = 'descending';
    }
    else{
      buttonData[sortKey] = 'ascending';
    }
    this.previousKey = sortKey;
    if(sortKey=='updated_at'){
      data.sort((a,b) => {
        if (Date(a[sortKey]) < Date(b[sortKey])) {
          return x;
        }
        if (Date(a[sortKey]) > Date(b[sortKey])) {
          return y;
        }
        return 0;
      });
    }
    else{
      data.sort((a,b) => {
        if (a[sortKey] < b[sortKey]) {
          return x;
        }
        if (a[sortKey] > b[sortKey]) {
          return y;
        }
        return 0;
      });
    }
    this.state = { 'photographers': data.slice(this.pageNumber*this.pageEntries,(this.pageNumber*this.pageEntries) + this.pageEntries),
                   'buttons': buttonData,};
    this.setState(this.state);
  }

  selectPage(){
    var number = parseInt(document.getElementById("pageSelection").value);
    if((number >= 0) && (number < (this.totalEntries/this.pageEntries))){
      this.pageNumber = number;
    }
    this.refreshPage();
  }

  flipPage(direction){
    if(direction == 'back' && this.pageNumber!=0){
      this.pageNumber--;
    }
    else if(direction == 'forward' && (this.pageNumber < (this.totalEntries/this.pageEntries) - 1)){
      this.pageNumber++;
    }
    this.refreshPage()
  }

  refreshPage(){
    this.state = { 'photographers': this.state.input.slice(this.pageNumber*this.pageEntries,(this.pageNumber*this.pageEntries) + this.pageEntries),
                   'pageStatus': "Displaying page " + this.pageNumber + " of " + Math.ceil((this.totalEntries/this.pageEntries)-1) + ", " + this.pageEntries + " entries per page"};
    document.getElementById("headerForm").reset();
    this.setState(this.state);
  }

  render() {
	var sortedObjects = this.state.photographers;
    return (
      <div className="photographer">
        <form id="headerForm"><span class="b">{this.state.pageStatus}</span>
          <span class="a">Photographers</span>
          <span class="c">
          <label>
            Page Number:  <input type="text" id="pageSelection" size='5' placeholder={this.pageNumber}/>
          </label>
          <button type="button" onClick={e => this.selectPage()}>Jump</button>
          <button type="button" onClick={e => this.flipPage('back')}>Back</button>
          <button type="button" onClick={e => this.flipPage('forward')}>Forward</button></span></form>
        <Table>
          <thead>
            <tr>
              <th>Profile Image</th>
              <th>
                Name  <button type="button" onClick={e => this.onSort('name')}>
                <img id='name' src={buttonIcons[this.state.buttons['name']]}
                width='20' height='20'/></button>
              </th>
              <th>
                Updated at  <button type="button" onClick={e => this.onSort('updated_at')}>
                <img id='updated_at' src={buttonIcons[this.state.buttons['updated_at']]}
                width='20' height='20'/></button>
              </th>
              <th>
                Total Likes  <button type="button" onClick={e => this.onSort('total_likes')}>
                <img id='total_likes' src={buttonIcons[this.state.buttons['total_likes']]}
                width='20' height='20'/></button>
              </th>
              <th>
                Total Photos  <button type="button" onClick={e => this.onSort('total_photos')}>
                <img id='total_photos' src={buttonIcons[this.state.buttons['total_photos']]}
                width='20' height='20'/></button>
              </th>
              <th>
                Total Collections  <button type="button" onClick={e => this.onSort('total_collections')}>
                <img id='total_collections' src={buttonIcons[this.state.buttons['total_collections']]}
                width='20' height='20'/></button>
              </th>
            </tr>
          </thead>
          <tbody>
            {sortedObjects.map((obj, index) => {
              return (
                <tr height={imageSize+25}>
                  <th scope="row" width={imageSize+25}>
                    <img src={obj.profile_image['large']}/>
                  </th>
                  <td width={imageSize}><a href={'/photographers/' + obj.id}>{obj.name}</a></td>
                  <td width={imageSize}>{convertDate(obj.updated_at)}</td>
                  <td width={imageSize}>{obj.total_likes}</td>
                  <td width={imageSize}>{obj.total_photos}</td>
                  <td width={imageSize}>{obj.total_collections}</td>
                </tr>
              )
            })}
          </tbody>
        </Table>
      </div>
    )
  }
}

function convertDate(s) {
	var date = new Date(s);
	return date.toDateString();
}


// retrieve display data that flask passed via html for us
const data = document.querySelector("#flask-data").dataset
const json = JSON.parse(data.json)

ReactDOM.render(
  <React.StrictMode>
    <div className="App" style={{ fontFamily: 'Consolas' }}>
    <div>
		<Navbar color="white" light expand="sm">
			<NavbarBrand href="/"> photoslayers </NavbarBrand>
      <NavbarText> <NavLink href="/covid19" style={{ color: 'orange' }}> COVID-19 Resources</NavLink> </NavbarText>
      <Nav className="mr-auto" navbar> </Nav>
      <NavbarText>
        <NavLink href="/search" style={{ color: 'black' }}>
          Search
        </NavLink>
      </NavbarText>
			<NavbarText> <NavLink href="/photos" style={{ color: 'black' }}> Photos </NavLink> </NavbarText>
			<NavbarText> <NavLink href="/photographers" style={{ color: 'black' }}> Photographers </NavLink> </NavbarText>
			<NavbarText> <NavLink href="/collections" style={{ color: 'black' }}> Collections </NavLink> </NavbarText>
			<NavbarText> <NavLink href = "/about" style={{ color: 'black' }}> About </NavLink> </NavbarText>
		</Navbar>
	</div>
      <PhotographersPageComponent data={json} />
    </div>
  </React.StrictMode>,
  document.getElementById('root')
)
