const { Button, Nav, Navbar, NavbarBrand, NavLink, NavbarText, Form, Table } = window.Reactstrap
var imageSize = 200

function convert(input){
	var i = 0;
	var output = "";
	var padding = 0;
	while(i<input.length){
		padding = 3 - input.charCodeAt(i).toString().length;
		while(padding > 0){
			output += "0";
			padding--;
		}
		output += input.charCodeAt(i).toString();
		i++;
	}
	return output;
}

class PhotographerComponent extends React.Component {
  render() {
    const {data} = this.props
  
    const items = [];
    var i = 0;
    while(i<data.photos.length){
      items.push(<a href={"/photos/" + convert(data.photos[i]['id'])}><img class='c' src={data.photos[i].urls['raw']} /></a>);
      i++;
    }

    return (
      <div className="photo">
      <span className="a">Photographers</span>
      <div className='columna' align="right"><Table><tbody><table align="left"><tr><th align="right">Preview Photos</th></tr>{items}</table></tbody></Table>
      </div>
      <div className='columnb' align="left">
        <Table>
          <tbody>
                <table>
                  <tr>
                    <th>Profile Image</th><td>
                      <img src={data.profile_image['large']}/>
                    </td>
                  </tr>
                  <tr>
                    <th>Name</th>
                    <td>{data.name}</td>
                  </tr>
                  <tr>
                    <th>Updated at</th>
                    <td>{convertDate(data.updated_at)}</td>
                  </tr>
                  <tr>
                    <th>Total Likes</th>
                    <td>{data.total_likes}</td>
                  </tr>
                  <tr>
                    <th>Total Photos</th>
                    <td>{data.total_photos}</td>
                  </tr>
                  <tr>
                    <th>Total Collections</th>
                    <td>{data.total_collections}</td>
                  </tr>
                  <tr>
                    <th>Location</th>
                    <td>{data.location}</td>
                  </tr>
                </table>
          </tbody>
        </Table>
        </div>
      </div>
    )
  }
}

function convertDate(s) {
	var date = new Date(s);
	return date.toDateString();
}

const data = document.querySelector("#flask-data").dataset
const json = JSON.parse(data.json)

ReactDOM.render(
  <React.StrictMode>
    <div className="App" style={{ fontFamily: 'Consolas' }}>
    <div>
		<Navbar color="white" light expand="sm">
			<NavbarBrand href="/"> photoslayers </NavbarBrand>
			<Nav className="mr-auto" navbar> </Nav>		
			<NavbarText> <NavLink href="/photos" style={{ color: 'black' }}> Photos </NavLink> </NavbarText>
			<NavbarText> <NavLink href="/photographers" style={{ color: 'black' }}> Photographers </NavLink> </NavbarText>
			<NavbarText> <NavLink href="/collections" style={{ color: 'black' }}> Collections </NavLink> </NavbarText>
			<NavbarText> <NavLink href = "/about" style={{ color: 'black' }}> About </NavLink> </NavbarText>
		</Navbar>
	</div>
      <PhotographerComponent data={json} />
    </div>
  </React.StrictMode>,
  document.getElementById('root')
)
