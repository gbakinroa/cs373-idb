const { Button, Nav, Navbar, NavbarBrand, NavLink, NavbarText, Form, Table } = window.Reactstrap

const bayo = '../assets/Bayo.jpeg'

const david = '../assets/david.png'

const lara = '../assets/lara.jpeg'

const zach = '../assets/zach.jpg'

const josh = '../assets/josh.JPG'

const dimensions = [[848, 1280], [584, 650], [1143, 1932], [2240, 3360], [2364,2364]];
const photoSize = 400;

function About() {
  return (
    <div className="splash" style={{ fontFamily: 'Consolas' }}>
      <h1> About Us </h1>

      <p>
        Hi! We are the students behind Photo Slayers! Find out more about us and our
        process down below!
      </p>

      <br />

      <h2> Who We Are </h2>

      <br />

      <h3> Adebayo Gbakinro </h3>

      <img src={bayo} alt="Bayo"
        width={preserveAspectRatio(0,0)}
        height={preserveAspectRatio(0,1)}
      />

      <p> Hi! My name is Bayo and I am a Fourth-Year Computer Science major! </p>

      <p> Responsibilities: Group Leader, Technical Report </p>

      <p> Number of Commits: 10, Number of Issues: 8, Number of Unit Tests: 0 </p>

      <br />

      <h3> David Benny </h3>

      <img src={david} alt="David"
        width={preserveAspectRatio(1,0)}
        height={preserveAspectRatio(1,1)}
      />

      <p>
        I am a junior student at the University of Texas at Austin currently pursuing my
        Bachelor of Science in Computer Science.
      </p>

      <p> Responsibilities: Scraping Data, Photo & Collection Pages, Instance Pages </p>

      <p> Number of Commits: 7, Number of Issues: 6, Number of Unit Tests: 0 </p>

      <br />

      <h3> Lara Flynn </h3>

      <img src={lara} alt="Lara"
        width={preserveAspectRatio(2,0)}
        height={preserveAspectRatio(2,1)}
      />

      <p>
        I am a Computer Science major going into my third year at The University of
        Texas at Austin in the fall of 2020. I am also pursuing a minor in Business and
        a certificate in Japanese language. I am interested in software development and
        its implications in the real world.
      </p>

      <p> Responsibilities: GCP Hosting, Splash Page, Initializing Flask Server, Database Design </p>

      <p> Number of Commits: 8, Number of Issues: 11, Number of Unit Tests: 0 </p>

      <br />

      <h3> Josh Guenther </h3>

      <img src={josh} alt="Josh"
        width={preserveAspectRatio(3,0)}
        height={preserveAspectRatio(3,1)}
      />

      <p>
        I became interested in Computer Science starting in high school. Throughout
        college, I participated in multiple Hackathon events both in Austin and College
        Station. In between semesters at UT, I completed internships with Visa and
        Amazon.
      </p>

      <p> Responsibilities: Restful API, Photographer Page, Unit Tests </p>

      <p> Number of Commits: 4, Number of Issues: 6, Number of Unit Tests: 0 </p>

      <br />

      <h3> Zach Hardesty </h3>

      <img src={zach} alt="Zach"
        width={preserveAspectRatio(4,0)}
        height={preserveAspectRatio(4,1)}
      />

      <p>
        I am now enrolled in the University of Texas at Austin computer science degree
        plan.
      </p>

      <p> Responsibilities: GUI Implementation, React </p>

      <p> Number of Commits: 22, Number of Issues: 5, Number of Unit Tests: 0 </p>

      <br />

      <br />

      <h2> Overall Statistics </h2>

      <h3> Total Number of Commits: 46, Total Number of Issues: 26, Total Number of Unit Tests: 0 </h3>

      <p><a className="App-link" href="https://documenter.getpostman.com/view/11798510/T17Q6k6H" style={{ color: 'teal' }}>
        Postman API Documentation
      </a></p>

      <p><a className="App-link" href="https://gitlab.com/gbakinroa/cs373-idb/-/issues" style={{ color: 'teal' }}>
        GitLab Issue Tracker
      </a></p>

      <p><a className="App-link" href="https://gitlab.com/gbakinroa/cs373-idb" style={{ color: 'teal' }}>
        GitLab Repository
      </a></p>

      <p><a className="App-link" href="https://gitlab.com/gbakinroa/cs373-idb/-/wikis/Technical-Report" style={{ color: 'teal' }}>
        GitLab Wiki
      </a></p>

      <p><a className="App-link" href="https://speakerdeck.com/gbakinroa/group-1-presentation" style={{ color: 'teal' }}>
        Speaker Deck Presentation
      </a></p>

      <br />

      <h2> Data Used </h2>

      <p><a className="App-link" href="https://unsplash.com/documentation" style={{ color: 'teal' }}>
        Unsplash API
      </a></p>

      <p> This API was used to gather information for all of the photos, photographers, and collections </p>

      <br />

      <h2> Tools Used </h2>

      <p> Slack, Zoom, Postman, SQLAlchemy, yUML, ReactJS, Flask, Python, Google Cloud Platform, Bootstrap, GitLab </p>

	  <br />

	  <h2> Unit Tests </h2>

	  <form action="/tests/" method="post">
		  <button name="button" type="submit">Click here to run unit tests</button>
	  </form>

    </div>
  )
}

function preserveAspectRatio(person, dimension){
  return (dimensions[person][dimension]*photoSize)/
  Math.max(dimensions[person][0], dimensions[person][1]);
}

ReactDOM.render(
  <React.StrictMode>
    <div className="App" style={{ fontFamily: 'Consolas' }}>
    <div>
		<Navbar color="white" light expand="sm">
			<NavbarBrand href="/"> photoslayers </NavbarBrand>
      <NavbarText> <NavLink href="/covid19" style={{ color: 'orange' }}> COVID-19 Resources</NavLink> </NavbarText>
      <Nav className="mr-auto" navbar> </Nav>
      <NavbarText>
        <NavLink href="/search" style={{ color: 'black' }}>
          Search
        </NavLink>
      </NavbarText>
			<NavbarText> <NavLink href="/photos" style={{ color: 'black' }}> Photos </NavLink> </NavbarText>
			<NavbarText> <NavLink href="/photographers" style={{ color: 'black' }}> Photographers </NavLink> </NavbarText>
			<NavbarText> <NavLink href="/collections" style={{ color: 'black' }}> Collections </NavLink> </NavbarText>
			<NavbarText> <NavLink href = "/about" style={{ color: 'black' }}> About </NavLink> </NavbarText>
		</Navbar>
	</div>

      <About />
    </div>
  </React.StrictMode>,
  document.getElementById('root')
)
