
ifeq ($(shell uname), Darwin)          # Apple
		PYTHON   := python3
		PIP      := pip3
		PYLINT   := pylint
		COVERAGE := coverage
		PYDOC    := pydoc3
		AUTOPEP8 := autopep8
else ifeq ($(shell uname -p), unknown) # Windows
		PYTHON   := python
		PIP      := pip3
		PYLINT   := pylint
		COVERAGE := coverage
		PYDOC    := python -m pydoc
		AUTOPEP8 := autopep8
else                                   # UTCS
		PYTHON   := python3
		PIP      := pip3
		PYLINT   := pylint3
		COVERAGE := coverage
		PYDOC    := pydoc3
		AUTOPEP8 := autopep8
endif

models.html: ./app/models.py
	$(PYDOC) -w app/models.py

IDB2.log:
	git log > IDB2.log

clean:
	rm -f  *.pyc
	rm -rf __pycache__
	rm -f  TestApp.tmp

TestApp.tmp: ./app/tests.py
	 $(COVERAGE) run --branch ./app/tests.py >  TestApp.tmp 2>&1
	 $(COVERAGE) report -m                   >> TestApp.tmp
	 cat TestApp.tmp
