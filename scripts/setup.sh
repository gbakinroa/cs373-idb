#!/usr/bin/env bash

##########################################################################
#  By Zach Hardesty <hello@zachhardesty.com> (https://zachhardesty.com)  #
##########################################################################

LABEL="$(tput bold)slayer-setup"
INFO="\n$LABEL"
WARN="\n$(tput setaf 3)$LABEL"
ERR="\n$(tput setaf 1)$LABEL"
RESET=$(tput sgr0)

if [[ ! $(command -v npm) ]]; then
    printf "$ERR: missing node/npm, please install and rerun$RESET\n"
    return
fi

if [[ ! $(command -v python) ]]; then
    printf "$ERR: missing python, please install and rerun$RESET\n"
    return
fi

if [[ ! $(command -v yarn) ]]; then
    printf "$WARN: missing yarn, installing globally via npm$RESET\n"
    npm install -g yarn
fi

printf "$INFO: installing front-end dependencies$RESET\n"
yarn --cwd ./react-app install

printf "$INFO: creating python virtual env$RESET\n"
python3 -m venv .venv

printf "$INFO: activating python virtual env$RESET\n"
source .venv/bin/activate

printf "$INFO: installing python's pip dependencies$RESET\n"
pip3 install -r requirements.txt
