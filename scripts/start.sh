#!/usr/bin/env bash

##########################################################################
#  By Zach Hardesty <hello@zachhardesty.com> (https://zachhardesty.com)  #
##########################################################################

LABEL="$(tput bold)ps-start"
INFO="\n$LABEL"
WARN="\n$(tput setaf 3)$LABEL"
ERR="\n$(tput setaf 1)$LABEL"
RESET=$(tput sgr0)

printf "$INFO: cleaning old js files"
yarn --cwd react-app clean

printf "$INFO: activating python virtual env$RESET\n"
source .venv/bin/activate

# https://flask.palletsprojects.com/en/1.1.x/quickstart/#rendering-templates
# https://flask.palletsprojects.com/en/1.1.x/quickstart/#routing
printf "$INFO: starting js file watcher and flask server. refresh the page to see changes\n\n"
yarn --cwd react-app run concurrently -n watch,flask 'yarn watch' 'python ../main.py'
# yarn --cwd react-app run concurrently -n watch,flask 'yarn watch' 'FLASK_APP=main FLASK_ENV=development FLASK_DEBUG=1 flask run --extra-files src/photographers/index.js'
