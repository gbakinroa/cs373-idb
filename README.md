# cs373-idb

## setting up your env

- download git-bash or any other bash shell as a terminal if on Windows
- download and install `nodejs` and `python3`
- run this script to install all dependencies: `. ./scripts/setup.sh`

## running the website

### live reload testing / dev w flask

```sh
. ./scripts/start.sh
```

## app info

[our postman page](https://documenter.getpostman.com/view/11798510/SzzrZaKF?version=latest) provides the information on public APIs

view the technical report for a detailed writeup [on the wiki page](https://gitlab.com/gbakinroa/cs373-idb/-/wikis/Technical-Report)

## references and help

### apis

- [unsplash](https://unsplash.com/documentation)
- [twitter](https://developer.twitter.com/en/docs)
- google maps

### running

- [reactstrap docs](https://reactstrap.github.io/)
- [flask setup](https://stackoverflow.com/a/45634550/5299167)
- [create-react-app docs](https://reactjs.org/docs/create-a-new-react-app.html)
