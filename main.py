import json
import os

from flask import Blueprint, Flask, redirect, render_template, request, send_file


#import tests

#from tests import app


def convert(input):
    if type(input) is int:
        return input
    #return "".join(f"{ord(x):03d}") for x in input)
    output = ""
    for char in input:
        output += ("{:03d}".format(ord(char)))
    return output


def is_static(path=""):
    if path != "" and os.path.exists(app.static_folder + "/" + path):
        return send_file(app.static_folder + "/" + path, cache_timeout=1)
    else:
        return False


app = Flask(__name__, static_folder="react-app/build")


# Serve React App
@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def serve(path):
    # if a file exists at a path, serve it over any Flask templates
    if path != "" and os.path.exists(app.static_folder + "/" + path):
        return send_file(app.static_folder + "/" + path, cache_timeout=1)
    else:
        return render_template("index.html", cache_timeout=1, at=app.static_folder)


@app.route("/about/")
def about():
    return render_template("index.html", cache_timeout=1, at=app.static_folder)


'''@app.route("/tests/", methods=["GET", "POST"])
def show_tests():
    import unittest
    import contextlib
    import io

    # redirect unittest output from terminal to string
    output = ""
    suite = unittest.TestLoader().loadTestsFromModule(tests)
    with io.StringIO() as buf:
        # run the tests and put everything into output string
        with contextlib.redirect_stdout(buf):
            unittest.TextTestRunner(stream=buf).run(suite)
        output = buf.getvalue()
    return render_template("index.html", tests_output=output)'''


@app.route("/covid19/")
def covid19():
    return render_template("index.html", cache_timeout=1, at=app.static_folder)


@app.route("/search/")
def search():
    queriesStr = request.args.get("q", None)

    if not queriesStr:
        return render_template(
            "index.html",
            cache_timeout=1,
            at=app.static_folder,
            json=json.dumps({"photos": [], "photographers": [], "collections": []}),
        )

    with open(app.static_folder + "/../../data/photos.json", "r") as filePhotos, open(
        app.static_folder + "/../../data/collections.json", "r"
    ) as fileCollections, open(
        app.static_folder + "/../../data/photographers.json", "r"
    ) as filePhotographers:
        # merge all data into single list
        items = json.load(filePhotos)
        items.update(json.load(fileCollections))
        items.update(json.load(filePhotographers))

        # translate all ids and add matches default
        for item in items["photos"]:
            item["id"] = convert(item["id"])
            item["matches"] = []
        for item in items["collections"]:
            item["id"] = convert(item["id"])
            item["matches"] = []
        for item in items["photographers"]:
            item["id"] = convert(item["id"])
            item["matches"] = []

        # search per word in query
        queries = queriesStr.split(" ")

        # look thru all data and record which queries match the item
        for item in items["photos"]:
            itemStr = json.dumps(item)
            for query in queries:
                if itemStr.find(query) != -1:
                    item["matches"].append(query)

        for item in items["photographers"]:
            itemStr = json.dumps(item)
            for query in queries:
                if itemStr.find(query) != -1:
                    item["matches"].append(query)

        for item in items["collections"]:
            itemStr = json.dumps(item)
            for query in queries:
                if itemStr.find(query) != -1:
                    item["matches"].append(query)

        # filter out items that match no search terms
        items["photos"] = [item for item in items["photos"] if len(item["matches"]) > 0]
        items["collections"] = [
            item for item in items["collections"] if len(item["matches"]) > 0
        ]
        items["photographers"] = [
            item for item in items["photographers"] if len(item["matches"]) > 0
        ]

        return render_template(
            "index.html", cache_timeout=1, at=app.static_folder, json=json.dumps(items),
        )


@app.route("/photos/")
def photos():
    with open(app.static_folder + "/../../data/photos.json", "r") as file:
        photos = json.load(file)

        for photo in photos["photos"]:
            photo["id"] = convert(photo["id"])
            # photo.user['id'] = convert(photo.user['id'])
            # Not working for some reason, so I converted it on the other end

        return render_template(
            "index.html",
            cache_timeout=1,
            at=app.static_folder,
            json=json.dumps(photos),
        )


@app.route("/collections/")
def collections():
    with open(app.static_folder + "/../../data/collections.json", "r") as file:
        collections = json.load(file)

        for collection in collections["collections"]:
            collection["id"] = convert(collection["id"])
            # collection.cover_photo['id'] = convert(collection.cover_photo['id'])
            # collection.user['id'] = convert(collection.user['id'])
            # Not working for some reason, so I converted it on the other end

        return render_template(
            "index.html",
            cache_timeout=1,
            at=app.static_folder,
            json=json.dumps(collections),
        )


@app.route("/photographers/")
def photographers():
    with open(app.static_folder + "/../../data/photographers.json", "r") as file:
        photographers = json.load(file)

        for photographer in photographers["photographers"]:
            photographer["id"] = convert(photographer["id"])

        return render_template(
            "index.html",
            cache_timeout=1,
            at=app.static_folder,
            json=json.dumps(photographers),
        )


@app.route("/photos/<int:id>/")
def photo(id):
    with open(app.static_folder + "/../../data/photos.json", "r") as file:
        photos = json.load(file)["photos"]
        for photo in photos:
            if int(convert(photo["id"])) == id:
                photo["id"] = int(convert(photo["id"]))
                return render_template(
                    "photo.html",
                    cache_timeout=1,
                    at=app.static_folder,
                    json=json.dumps(photo),
                )
        return redirect("/photos/")


@app.route("/photographers/<int:id>/")
def photographer(id):
    with open(app.static_folder + "/../../data/photographers.json", "r") as file:
        photographers = json.load(file)["photographers"]
        for photographer in photographers:
            if int(convert(photographer["id"])) == id:
                photographer["id"] = int(convert(photographer["id"]))
                return render_template(
                    "photographer.html",
                    cache_timeout=1,
                    at=app.static_folder,
                    json=json.dumps(photographer),
                )
        return redirect("/photographers/")


@app.route("/collections/<int:id>/")
def collection(id):
    with open(app.static_folder + "/../../data/collections.json", "r") as file:
        collections = json.load(file)["collections"]
        for collection in collections:
            if collection["id"] == id:
                collection["id"] = int(convert(collection["id"]))
                return render_template(
                    "collection.html",
                    cache_timeout=1,
                    at=app.static_folder,
                    json=json.dumps(collection),
                )
        return redirect("/collections/")


@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers["Cache-Control"] = "public, max-age=0"

    return r


# Start defining API routes
RestAPI = Blueprint("RestAPI", __name__, url_prefix="/api/v1")

DEFAULT_OFFSET = 0
DEFAULT_LIMIT = 30


@RestAPI.route("/photographers/<int:id>/")
def photographer_details(id):
    """
    Get details about a photographer
    """
    with open(app.static_folder + "/../../data/photographers.json", "r") as file:
        photographers = json.load(file)["photographers"]
        for photographer in photographers:
            if int(convert(photographer["id"])) == id:
                photographer["id"] = int(convert(photographer["id"]))
                response = {
                    "id": photographer["id"],
                    "profile_image": photographer["profile_image"],
                    "name": photographer["name"],
                    "updated_at": photographer["updated_at"],
                    "total_likes": photographer["total_likes"],
                    "total_photos": photographer["total_photos"],
                    "total_collections": photographer["total_collections"],
                    "location": photographer["location"],
                    "photos": photographer["photos"]
                }
    return response, 404


@RestAPI.route("/photographers/")
def all_photographers():
    """
    Get a paginated list of photographers
    """
    offset = request.args.get("offset")
    if offset is None:
        offset = DEFAULT_OFFSET

    limit = request.args.get("limit")
    if limit is None:
        limit = DEFAULT_LIMIT
    response = {
        "offset": offset,
        "limit": limit,
        "photographers": [],
    }

    # result = db.session.query(Photographer).limit(limit).offset(offset).all()
    # for model in result:
    #     response["photographers"].append(toDict(model))

    return response, 200


@RestAPI.route("/photos/<int:id>/")
def photo_details(id):
    """
    Get details about a photo
    """
    with open(app.static_folder + "/../../data/photos.json", "r") as file:
        photos = json.load(file)["photos"]
        for photo in photos:
            if int(convert(photo["id"])) == id:
                photo["id"] = int(convert(photo["id"]))
                response = {
                    "id": photo["id"],
                    "user": photo["user"],
                    "likes": photo["likes"],
                    "created_at": photo["created_at"],
                    "updated_at": photo["updated_at"],
                    "tags": photo["tags"]
                }
    return response, 404


@RestAPI.route("/photos/")
def all_photos():
    """
    Get a paginated list of photos
    """
    offset = request.args.get("offset")
    if offset is None:
        offset = DEFAULT_OFFSET

    limit = request.args.get("limit")
    if limit is None:
        limit = DEFAULT_LIMIT
    response = {
        "offset": offset,
        "limit": limit,
        "photos": [],
    }

    # result = db.session.query(Photo).limit(limit).offset(offset).all()
    # for model in result:
    #     response["photos"].append(toDict(model))

    return response, 200


@RestAPI.route("/collections/<int:id>/")
def collection_details(id):
    """
    Get details about a collection
    """
    with open(app.static_folder + "/../../data/collections.json", "r") as file:
        collections = json.load(file)["collections"]
        for collection in collections:
            if collection["id"] == id:
                collection["id"] = int(convert(collection["id"]))
                response = {
                    "id": collection["id"],
                    "title": collection["title"],
                    "user": collection["user"],
                    "total_photos": collection["total_photos"],
                    "published_at": collection["published_at"],
                    "updated_at": collection["updated_at"],
                    "tags": collection["tags"],
                    "preview_photos": collection["preview_photos"]
                }
    return response, 404


@RestAPI.route("/collections/")
def all_collections():
    """
    Get a paginated list of collections
    """
    offset = request.args.get("offset")
    if offset is None:
        offset = DEFAULT_OFFSET

    limit = request.args.get("limit")
    if limit is None:
        limit = DEFAULT_LIMIT
    response = {
        "offset": offset,
        "limit": limit,
        "collections": [],
    }

    # result = db.session.query(Collection).limit(limit).offset(offset).all()
    # for model in result:
    #     response["collections"].append(toDict(model))

    return response, 200


# Add API routes to web app
app.register_blueprint(RestAPI)


def main():
    """
    Main function to initialize the app
    """
    app.run(
        port=5000, debug="true", extra_files="react-app/src/photographers/index.js",
    )


if __name__ == "__main__":
    main()
