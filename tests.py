import os
import sys
import unittest
from sqlalchemy import cast, ARRAY, String, func

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__, static_folder="react-app/build")
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres+psycopg2://postgres:asd123@localhost:5432')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
app.app_context().push()
db = SQLAlchemy()
db.init_app(app)

class Photographer(db.Model):
    __tablename__ = 'photographer'
    
    id = db.Column(
        db.Integer,
        primary_key = True,
        nullable = False,
        unique = True
    )
    
    name = db.Column(
        db.String(),
        nullable = False
    )
    
    likes = db.Column(
        db.Integer
    )
    
    photos = db.Column(
        db.ARRAY(db.Integer),
        nullable = False
    )
    
    albums = db.Column(
        db.ARRAY(db.Integer)
    )
    
    # relationships
    photo = db.relationship('Photo', back_populates = 'photographer') # 1-many
    
    collection = db.relationship('Collection', back_populates = 'photographer') # 1-many

class Photo(db.Model):
    __tablename__ = 'photo'
    
    id = db.Column(
        db.Integer,
        primary_key = True,
        nullable = False,
        unique = True
    )
    
    camera_make = db.Column(
        db.String()
    )
    
    camera_model = db.Column(
        db.String()
    )
    
    location_latitude = db.Column(
        db.Float
    )
    
    location_longitude = db.Column(
        db.Float
    )
    
    date_created = db.Column(
        db.String()
    )
    
    date_modified = db.Column(
        db.String()
    )
    
    # relationships
    photographer_id = db.Column(
        db.Integer,
        db.ForeignKey('photographer.id'),
        nullable = False
    )
    
    photographer = db.relationship('Photographer', back_populates = 'photo') # many-1
    
    collection = db.relationship('Collection', back_populates = 'photo') # 1-many

class Collection(db.Model):
    __tablename__ = 'collection'
    
    id = db.Column(
        db.Integer,
        primary_key = True,
        nullable = False,
        unique = True
    )
    
    title = db.Column(
        db.String(),
        nullable = False
    )
    
    tags = db.Column(
        db.ARRAY(db.String())
    )
    
    curator_id = db.Column(
        db.Integer
    )
    
    curator_name = db.Column(
        db.String()
    )
    
    photos = db.Column(
        db.ARRAY(db.Integer),
        nullable = False
    )
    
    date_created = db.Column(
        db.String()
    )
    
    date_modified = db.Column(
        db.String()
    )
    
    # relationships
    photo_id = db.Column(
        db.Integer, 
        db.ForeignKey('photo.id')
    )
    
    photo = db.relationship('Photo', back_populates = 'collection') # many-1
    
    photographer_id = db.Column(
        db.Integer,
        db.ForeignKey('photographer.id'),
        nullable = False
    )
    
    photographer = db.relationship('Photographer', back_populates = 'collection') # many-1

db.drop_all()
db.create_all()

class DBTestCases(unittest.TestCase):

    # format: 2 fields tests, 2 relationship tests with other 2 models
    # note: for photo and collection, always add photographer first and delete photographer last so photographer_id can be found
    
    # unit tests for Photo model
    def test_source_insert_1(self):
        pg = Photographer(id = 15, name = 'Guy', photos = [9, 0, 1])
        db.session.add(pg)
        db.session.commit()
        
        s = Photo(id = 23, photographer_id = 15, camera_make = 'Kodak', date_created = '10-02-19')
        db.session.add(s)
        db.session.commit()
        
        r = db.session.query(Photo).filter_by(id = 23, photographer_id = 15,  camera_make = 'Kodak', date_created = '10-02-19').one()
        self.assertEqual(r.id, 23)
        self.assertEqual(r.photographer_id, 15)
        self.assertEqual(r.camera_make, 'Kodak')
        self.assertEqual(r.date_created, '10-02-19')
        
        db.session.query(Photo).filter_by(id = 23, photographer_id = 15,  camera_make = 'Kodak', date_created = '10-02-19').delete()
        db.session.commit()
        db.session.query(Photographer).filter_by(id = 15, name = 'Guy', photos = [9, 0, 1]).delete()
        db.session.commit()
    
    def test_source_insert_2(self):
        pg = Photographer(id = 1738, name = 'Guy', photos = [9, 0, 1])
        db.session.add(pg)
        db.session.commit()
        
        s = Photo(id = 363, camera_model = 'Paris', date_modified = '07-31-18', location_longitude = 158.46239, location_latitude = -63.44932, photographer_id = 1738)
        db.session.add(s)
        db.session.commit()
        
        r = db.session.query(Photo).filter_by(id = 363, camera_model = 'Paris', date_modified = '07-31-18', location_longitude = 158.46239, location_latitude = -63.44932, photographer_id = 1738).one()
        self.assertEqual(r.id, 363)
        self.assertEqual(r.camera_model, 'Paris')
        self.assertEqual(r.date_modified, '07-31-18')
        self.assertEqual(r.location_longitude, 158.46239)
        self.assertEqual(r.location_latitude, -63.44932)
        self.assertEqual(r.photographer_id, 1738)
        
        db.session.query(Photo).filter_by(id = 363, camera_model = 'Paris', date_modified = '07-31-18', location_longitude = 158.46239, location_latitude = -63.44932, photographer_id = 1738).delete()
        db.session.commit()
        db.session.query(Photographer).filter_by(id = 1738, name = 'Guy', photos = [9, 0, 1]).delete()
        db.session.commit()
    
    # relationship test with photographer
    def test_source_insert_3_a(self):
        add1 = Photographer(id = 394, name = 'Guy', photos = [9, 0, 1])
        db.session.add(add1)
        db.session.commit()
        
        add2 = Photo(id = 3, date_created = '01-01-01', photographer_id = 394)
        db.session.add(add1)
        db.session.commit()
        
        self.assertEqual(add1.id, add2.photographer_id)    
        
        db.session.query(Photo).filter_by(id = 394, date_created = '01-01-01', photographer_id = 394).delete()
        db.session.commit()
        db.session.query(Photographer).filter_by(id = 783, name = 'Guess', photos = [8]).delete()
        db.session.commit()
        
    # relationship test with collection
    def test_source_insert_3_b(self):
        pg = Photographer(id = 321, name = 'Guy', photos = [9, 0, 1])
        db.session.add(pg)
        db.session.commit()
        
        add1 = Photo(id = 83, date_created = '01-02-01', photographer_id = 321)
        db.session.add(add1)
        db.session.commit()
        
        add2 = Collection(title = 'frogs', photos = [0, 1], photo_id = 83, photographer_id = 321)
        db.session.add(add2)
        db.session.commit()
        
        add3 = Collection(title = 'rabbits', photos = [2, 3], photo_id = 83, photographer_id = 321)
        db.session.add(add3)
        db.session.commit()
        
        self.assertEqual(add1.id, add2.photo_id)
        self.assertEqual(add1.id, add3.photo_id)
        
        db.session.query(Collection).filter_by(title = 'frogs', photos = [0, 1], photo_id = 83, photographer_id = 321).delete()
        db.session.commit()
        db.session.query(Collection).filter_by(title = 'rabbits', photos = [2, 3], photo_id = 83, photographer_id = 321).delete()
        db.session.commit()
        db.session.query(Photo).filter_by(id = 83, date_created = '01-02-01', photographer_id = 321).delete()
        db.session.commit()
        db.session.query(Photographer).filter_by(id = 321, name = 'Guy', photos = [9, 0, 1]).delete()
        db.session.commit()
    
    # unit tests for Photographer model    
    def test_source_insert_4(self):
        s = Photographer(id = 20, name = 'Gloria', likes = 49, photos = [1, 2, 3])
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Photographer).filter_by(id = 20, name = 'Gloria', likes = 49, photos = [1, 2, 3]).one()
        self.assertEqual(r.id, 20)
        self.assertEqual(r.name, 'Gloria')
        self.assertEqual(r.likes, 49)
        self.assertEqual(r.photos, [1, 2, 3])

        db.session.query(Photographer).filter_by(id = 20, name = 'Gloria', likes = 49, photos = [1, 2, 3]).delete()
        db.session.commit()
    
    def test_source_insert_5(self):
        s = Photographer(id = 93, name = 'Gyro', photos = [42069, 96024, 5203], albums = [43, 5, 6, 7])
        db.session.add(s)
        db.session.commit()
        
        r = db.session.query(Photographer).filter_by(id = 93, name = 'Gyro', photos = [42069, 96024, 5203], albums = [43, 5, 6, 7]).one()
        self.assertEqual(r.id, 93)
        self.assertEqual(r.name, 'Gyro')
        self.assertEqual(r.photos, [42069, 96024, 5203])
        self.assertEqual(r.albums, [43, 5, 6, 7])
        
        db.session.query(Photographer).filter_by(id = 93, name = 'Gyro', photos = [42069, 96024, 5203], albums = [43, 5, 6, 7]).delete()
        db.session.commit()
    
    # relationship test with photo
    def test_source_insert_6_a(self):
        add1 = Photographer(id = 8934, name = 'Yes', photos = [29, 83, 10])
        db.session.add(add1)
        db.session.commit()
        
        add2 = Photo(id = 3, date_created = '01-01-01', photographer_id = 8934)
        db.session.add(add2)
        db.session.commit()
        
        self.assertEqual(add1.id, add2.photographer_id)    
        
        db.session.query(Photo).filter_by(id = 3, date_created = '01-01-01', photographer_id = 8934).delete()
        db.session.commit()
        db.session.query(Photographer).filter_by(id = 8934, name = 'Yes', photos = [29, 83, 10]).delete()
        db.session.commit()
    
    # relationship test with collection
    def test_source_insert_6_b(self):
        add1 = Photographer(id = 985222, name = 'Ginny', photos = [0, 1, 90])
        db.session.add(add1)
        db.session.commit()
        
        add2 = Collection(id = 3, title = 'guitar', photos = [8, 9, 11], date_created = '01-01-01', photographer_id = 985222)
        db.session.add(add2)
        db.session.commit()
        
        self.assertEqual(add1.id, add2.photographer_id)    
        
        db.session.query(Collection).filter_by(id = 3, title = 'guitar', photos = [8, 9, 11], date_created = '01-01-01', photographer_id = 985222).delete()
        db.session.commit()
        db.session.query(Photographer).filter_by(id = 985222, name = 'Ginny', photos = [0, 1, 90]).delete()
        db.session.commit()
    
    # unit tests for Collection model
    def test_source_insert_7(self):
        pg = Photographer(id = 20384, photos = [83, 92, 10], name = 'Gilbert')
        db.session.add(pg)
        db.session.commit()
    
        # NOTE: for arrays of strings, cast it to string array first; otherwise it will try to compare it to varchar array and fail
        # if you do that, make sure to delete with this parameter: synchronize_session = 'fetch'
        s = Collection(title = 'dogs', tags = cast(['retriever', 'chihuahua', 'shihtzu'], ARRAY(String)), photographer_id = 20384, photos = [4, 5, 6])
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Collection).filter_by(title = 'dogs', tags = cast(['retriever', 'chihuahua', 'shihtzu'], ARRAY(String)), photographer_id = 20384, photos = [4, 5, 6]).one()
        self.assertEqual(r.title, 'dogs')
        self.assertEqual(r.tags, ['retriever', 'chihuahua', 'shihtzu'])
        self.assertEqual(r.photos, [4, 5, 6])
        self.assertEqual(r.photographer_id, 20384)

        db.session.query(Collection).filter_by(title = 'dogs', tags = cast(['retriever', 'chihuahua', 'shihtzu'], ARRAY(String)), photographer_id = 20384, photos = [4, 5, 6]).delete(synchronize_session = 'fetch')
        db.session.commit()
        db.session.query(Photographer).filter_by(id = 20384, photos = [83, 92, 10], name = 'Gilbert').delete()
        db.session.commit()

    def test_source_insert_8(self):
        pg = Photographer(id = 29, photos = [831, 921, 100], name = 'Gilbert546')
        db.session.add(pg)
        db.session.commit()
        
        s = Collection(title = 'cats', curator_id = 9, curator_name = 'guinea', date_created = '10-11-12', date_modified = '01-09-16', photographer_id = 29, photos = [7, 8, 9])
        db.session.add(s)
        db.session.commit()
        
        r = db.session.query(Collection).filter_by(title = 'cats', curator_id = 9, curator_name = 'guinea', date_created = '10-11-12', date_modified = '01-09-16', photos = [7, 8, 9], photographer_id = 29).one()
        self.assertEqual(r.title, 'cats')
        self.assertEqual(r.curator_id, 9)
        self.assertEqual(r.curator_name, 'guinea')
        self.assertEqual(r.date_created, '10-11-12')
        self.assertEqual(r.date_modified, '01-09-16')
        self.assertEqual(r.photos, [7, 8, 9])
        self.assertEqual(r.photographer_id, 29)
        
        db.session.query(Collection).filter_by(title = 'cats', curator_id = 9, curator_name = 'guinea', date_created = '10-11-12', date_modified = '01-09-16', photos = [7, 8, 9], photographer_id = 29).delete()
        db.session.commit()
        db.session.query(Photographer).filter_by(id = 29, photos = [831, 921, 100], name = 'Gilbert546').delete()
        db.session.commit()
           
    
    # relationship test with photo
    def test_source_insert_9_a(self):
        pg1 = Photographer(id = 872, photos = [34, 943, 91, 10, 0], name = '1234567890')
        db.session.add(pg1)
        db.session.commit()
        
        pg2 = Photographer(id = 4, photos = [34, 943, 91, 10, 0], name = '0987654321')
        db.session.add(pg2)
        db.session.commit()
        
        add1 = Photo(id = 985222, photographer_id = 872)
        db.session.add(add1)
        db.session.commit()
        
        add2 = Collection(id = 35, title = 'drums', photos = [8, 9, 12], date_created = '01-01-01', photographer_id = 4, photo_id = 985222)
        db.session.add(add2)
        db.session.commit()
        
        self.assertEqual(add1.id, add2.photo_id)    
        
        db.session.query(Collection).filter_by(id = 35, title = 'drums', photos = [8, 9, 12], date_created = '01-01-01', photographer_id = 4, photo_id = 985222).delete()
        db.session.commit()
        db.session.query(Photo).filter_by(id = 985222, photographer_id = 872).delete()
        db.session.commit()
        db.session.query(Photographer).filter_by(id = 4, photos = [34, 943, 91, 10, 0], name = '0987654321').delete()
        db.session.commit()
        db.session.query(Photographer).filter_by(id = 872, photos = [34, 943, 91, 10, 0], name = '1234567890').delete()
        db.session.commit()
    
    # relationship test with photographer
    def test_source_insert_9_b(self):   
        pg1 = Photographer(id = 872, photos = [34, 943, 91, 10, 0], name = '1234567890')
        db.session.add(pg1)
        db.session.commit()
        
        add1 = Photo(id = 834, photographer_id = 872)
        db.session.add(add1)
        db.session.commit()
        
        add2 = Collection(id = 35, title = 'drums', photos = [8, 9, 12], date_created = '01-01-01', photographer_id = 872, photo_id = 834)
        db.session.add(add2)
        db.session.commit()
        
        self.assertEqual(pg1.id, add1.photographer_id)    
        
        db.session.query(Collection).filter_by(id = 35, title = 'drums', photos = [8, 9, 12], date_created = '01-01-01', photographer_id = 872, photo_id = 834).delete()
        db.session.commit()
        db.session.query(Photo).filter_by(id = 834, photographer_id = 872).delete()
        db.session.commit()
        db.session.query(Photographer).filter_by(id = 872, photos = [34, 943, 91, 10, 0], name = '1234567890').delete()
        db.session.commit()
        
        db.drop_all()
        db.create_all()
    
if __name__ == '__main__':
    db.drop_all()
    db.create_all()
    unittest.main()
