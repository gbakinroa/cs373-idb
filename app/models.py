from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres+psycopg2://postgres:asd123@localhost:5432')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

class Photographer(db.Model):
    __tablename__ = 'photographer'
    
    id = db.Column(
        db.Integer,
        primary_key = True,
        nullable = False,
        unique = True
    )
    
    name = db.Column(
        db.String(),
        nullable = False
    )
    
    likes = db.Column(
        db.Integer
    )
    
    photos = db.Column(
        db.ARRAY(db.Integer),
        nullable = False
    )
    
    albums = db.Column(
        db.ARRAY(db.Integer)
    )
    
    # relationships
    photo = db.relationship('Photo', back_populates = 'photographer') # 1-many
    
    collection = db.relationship('Collection', back_populates = 'photographer') # 1-many

class Photo(db.Model):
    __tablename__ = 'photo'
    
    id = db.Column(
        db.Integer,
        primary_key = True,
        nullable = False,
        unique = True
    )
    
    camera_make = db.Column(
        db.String()
    )
    
    camera_model = db.Column(
        db.String()
    )
    
    location_latitude = db.Column(
        db.Float
    )
    
    location_longitude = db.Column(
        db.Float
    )
    
    date_created = db.Column(
        db.String()
    )
    
    date_modified = db.Column(
        db.String()
    )
    
    # relationships
    photographer_id = db.Column(
        db.Integer,
        db.ForeignKey('photographer.id'),
        nullable = False
    )
    
    photographer = db.relationship('Photographer', back_populates = 'photo') # many-1
    
    collection = db.relationship('Collection', back_populates = 'photo') # 1-many

class Collection(db.Model):
    __tablename__ = 'collection'
    
    id = db.Column(
        db.Integer,
        primary_key = True,
        nullable = False,
        unique = True
    )
    
    title = db.Column(
        db.String(),
        nullable = False
    )
    
    tags = db.Column(
        db.ARRAY(db.String())
    )
    
    curator_id = db.Column(
        db.Integer
    )
    
    curator_name = db.Column(
        db.String()
    )
    
    photos = db.Column(
        db.ARRAY(db.Integer),
        nullable = False
    )
    
    date_created = db.Column(
        db.String()
    )
    
    date_modified = db.Column(
        db.String()
    )
    
    # relationships
    photo_id = db.Column(
        db.Integer, 
        db.ForeignKey('photo.id')
    )
    
    photo = db.relationship('Photo', back_populates = 'collection') # many-1
    
    photographer_id = db.Column(
        db.Integer,
        db.ForeignKey('photographer.id'),
        nullable = False
    )
    
    photographer = db.relationship('Photographer', back_populates = 'collection') # many-1

db.drop_all()
db.create_all()